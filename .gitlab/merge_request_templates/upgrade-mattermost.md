<!-- After merging changes to this template, update the `Default description template for merge requests` -->
<!-- found under Settings - General Merge Requests -->
## What does this MR do?

<!-- Briefly describe what this MR is about. -->

%{first_multiline_commit}

## Related issues

<!-- Link related issues below. Insert the issue link or reference after the word "Closes" if merging this should automatically close it. -->

## Test plan

### Build tests

- [ ] Build on all supported platforms
- [ ] Run `Trigger:ee-package` and then `qa-test` CI jobs on `gitlab.com`.

### Fresh installation tests

Install a GitLab package created in the build system on a fresh OS installation
and run the following actions, checks, and tests:

- [ ] Install GitLab package with the new Mattermost version:

  - [ ] Verify that package installation logs/output shows no errors.
  - [ ] Verify Mattermost version by running `/opt/gitlab/embedded/bin/mattermost version`.

- [ ] Edit `/etc/gitlab/gitlab.rb` and set:

  - `external_url`
  - `mattermost_external_url`

  Both URLs should point to the same system so that GitLab and Mattermost are co-located. Example:

  ```yaml
  external_url 'gitlab.example.com'
  mattermost_external_url 'mattermost.example.com'
  ```

- [ ] Run `gitlab-ctl reconfigure`.
- [ ] Connect to `gitlab.example.com`
- [ ] Navigate to `Admin>Settings>Network>Outbound requests` and add `mattermost.example.com` to `Local IP addresses and domain names that hooks and services can accesss` and click `Save changes`.
- [ ] Navigate to `mattermost.example.com`.
- [ ] Verify that single-sign on using GitLab credentials is working:

  1. Click on `or create and account with` GitLab.
  1. When presented with the `Authorize GitLab Mattermost to use your account?` page, click on `Authorize`.
  1. You should land on the `Select teams` page.

- [ ] Verify that when creating a group in GitLab, checking the box for **Create a Mattermost team for this group** also creates a team in Mattermost and the GitLab user is a member of that team.
- [ ] Create a test project within the group created in the previous step and initialize with a `README`.

- [ ] Verify Mattermost slash command operation:
  - [ ] Enable slash commands using [GitLab documentation](https://docs.gitlab.com/ee/user/project/integrations/mattermost_slash_commands.html#configure-automatically).
  - [ ] Test slash commands by creating a new issue from the Mattermost instance. After following the prompt to re-authorize, the issue should be successfully created in GitLab.

- [ ] Verify GitLab issue notification.

  - [ ] Configure incoming web hooks in Mattermost using the [GitLab Documentation](https://docs.gitlab.com/ee/user/project/integrations/mattermost.html). Note that you have to configure both Mattermost and GitLab.
  - [ ] Using the created web hook, follow the documentation for adding [notification support](https://docs.gitlab.com/ee/user/project/integrations/mattermost.html#configure-mattermost-to-receive-gitlab-notifications).
  - [ ] Create an issue in the test project. Verify that the notification for the issue appears in Mattermost for the GitLab user.

### Upgrade installation tests

Install a GitLab package from the previous, latest minor number release on a
fresh OS installation. Run the following actions, checks, and tests:

- [ ] Install GitLab package from the previous, latest minor number release:

  - [ ] Verify that package installation logs/output shows no errors.
  - [ ] Verify Mattermost version by running `/opt/gitlab/embedded/bin/mattermost version`.

- [ ] Edit `/etc/gitlab/gitlab.rb` and set:

  - `external_url`
  - `mattermost_external_url`

  Both URLs should point to the same system so that GitLab and Mattermost are co-located. Example:

  ```yaml
  external_url 'gitlab.example.com'
  mattermost_external_url 'mattermost.example.com'
  ```

- [ ] Run `gitlab-ctl reconfigure`.
- [ ] Connect to `gitlab.example.com`
- [ ] Navigate to `Admin>Settings>Network>Outbound requests` and add `mattermost.example.com` to `Local IP addresses and domain names that hooks and services can accesss` and click `Save changes`.
- [ ] Navigate to `mattermost.example.com`.
- [ ] Verify that single-sign on using GitLab credentials is working:

  1. Click on `or create and account with` GitLab.
  1. When presented with the `Authorize GitLab Mattermost to use your account?` page, click on `Authorize`.
  1. You should land on the `Select teams` page.

- [ ] Verify that when creating a group in GitLab, checking the box for **Create a Mattermost team for this group** also creates a team in Mattermost and the GitLab user is a member of that team.
- [ ] Create a test project within the group created in the previous step and initialize with a `README`.

- [ ] Verify Mattermost slash command operation:

  - [ ] Enable slash commands using [GitLab documentation](https://docs.gitlab.com/ee/user/project/integrations/mattermost_slash_commands.html#configure-automatically).
  - [ ] Test slash commands by creating a new issue from the Mattermost instance. After following the prompt to re-authorize, the issue should be successfully created in GitLab.

- [ ] Verify GitLab issue notification.

  - [ ] Configure incoming web hooks in Mattermost using the [GitLab Documentation](https://docs.gitlab.com/ee/user/project/integrations/mattermost.html). Note that you have to configure both Mattermost and GitLab.
  - [ ] Using the created web hook, follow the documentation for adding [notification support](https://docs.gitlab.com/ee/user/project/integrations/mattermost.html#configure-mattermost-to-receive-gitlab-notifications).
  - [ ] Create an issue in the test project. Verify that the notification for the issue appears in Mattermost for the GitLab user.

Upgrade GitLab with a package created with the new Mattermost version. Run the
following actions, checks, and tests:

- [ ] Upgraded to package with new Mattermost version:

  - [ ] Verify that package installation logs/output shows no errors.
  - [ ] Verify Mattermost version by running `/opt/gitlab/embedded/bin/mattermost version`.

- [ ] Verify Mattermost slash command operation:

  - [ ] Test slash commands by creating a new issue from the Mattermost instance. After following the prompt to re-authorize, the issue should be successfully created in GitLab.

- [ ] Verify GitLab issue notification.

  - [ ] Create an issue in the test project. Verify that the notification for the issue appears in Mattermost for the GitLab user.


## Checklist

See [Definition of done](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/CONTRIBUTING.md#definition-of-done).

For anything in this list which will not be completed, please provide a reason in the MR discussion.

### Required

- [ ] MR title and description are up to date, accurate, and descriptive.
- [ ] MR targeting the appropriate branch.
- [ ] Latest Merge Result pipeline is green.
- [ ] When ready for review, MR is labeled "~workflow::ready for review" per the [Distribution MR workflow](https://about.gitlab.com/handbook/engineering/development/enablement/systems/distribution/merge_requests.html).

#### For GitLab team members

If you don't have access to this, the reviewer should trigger these jobs for you during the review process.

- [ ] The manual `Trigger:ee-package` jobs have a green pipeline running against latest commit.
- [ ] If `config/software` or `config/patches` directories are changed, make sure the `build-package-on-all-os` job within the `Trigger:ee-package` downstream pipeline succeeded.
- [ ] If you are changing anything SSL related, then the `Trigger:package:fips` manual job within the `Trigger:ee-package` downstream pipeline must succeed.
- [ ] If CI configuration is changed, the branch must be pushed to [`dev.gitlab.org`](https://dev.gitlab.org/gitlab/omnibus-gitlab) to confirm regular branch builds aren't broken.

### Expected (please provide an explanation if not completing)

- [ ] Test plan indicating conditions for success has been posted and passes.
- [ ] Documentation created/updated.
- [ ] Tests added.
- [ ] Integration tests added to [GitLab QA](https://gitlab.com/gitlab-org/gitlab-qa).
- [ ] Equivalent MR/issue for the [GitLab Chart](https://gitlab.com/gitlab-org/charts/gitlab) opened.
- [ ] Validate potential values for new configuration settings. Formats such as integer `10`, duration `10s`, URI `scheme://user:passwd@host:port` may require quotation or other special handling when rendered in a template and written to a configuration file.
