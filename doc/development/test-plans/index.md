---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Omnibus GitLab test plans

## Updating components

<!-- Keep this list sorted alphabetically. -->
- [libre2](libre2-upgrade.md)
- [Mattermost](mattermost-upgrade.md)
