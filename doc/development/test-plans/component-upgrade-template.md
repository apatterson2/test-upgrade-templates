---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# `<component name>` component upgrade test plan

<!-- Copy and paste the following into your MR description. -->
## Test plan

<!-- -
Add the component test-plan here. Use "[ ]" for each step/task to be completed.

At a minimum, the following test should be run:

- [ ] Perform a successful GitLab Enterprise Edition (EE) build on all supported platforms.
- [ ] Run `qa-test` CI test job for both GitLab Enterprise and GitLab Free editions.
- [ ] Install and verify that component version has been upgraded.
- [ ] Verify basic functionality of the software component.
-->
