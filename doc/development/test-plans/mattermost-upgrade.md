---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Mattermost component upgrade test plan

<!-- Copy and paste the following into your MR description. -->
## Test plan

### Build tests

- [ ] Build on all supported platforms
- [ ] Run `Trigger:ee-package` and then `qa-test` CI jobs on `gitlab.com`.

### Fresh installation tests

Install a GitLab package created in the build system on a fresh OS installation
and run the following actions, checks, and tests:

- [ ] Install GitLab package with the new Mattermost version:

  - [ ] Verify that package installation logs/output shows no errors.
  - [ ] Verify Mattermost version by running `/opt/gitlab/embedded/bin/mattermost version`.

- [ ] Edit `/etc/gitlab/gitlab.rb` and set:

  - `external_url`
  - `mattermost_external_url`

  Both URLs should point to the same system so that GitLab and Mattermost are co-located. Example:

  ```yaml
  external_url 'gitlab.example.com'
  mattermost_external_url 'mattermost.example.com'
  ```

- [ ] Run `gitlab-ctl reconfigure`.
- [ ] Connect to `gitlab.example.com`
- [ ] Navigate to `Admin>Settings>Network>Outbound requests` and add `mattermost.example.com` to `Local IP addresses and domain names that hooks and services can accesss` and click `Save changes`.
- [ ] Navigate to `mattermost.example.com`.
- [ ] Verify that single-sign on using GitLab credentials is working:

  1. Click on `or create and account with` GitLab.
  1. When presented with the `Authorize GitLab Mattermost to use your account?` page, click on `Authorize`.
  1. You should land on the `Select teams` page.

- [ ] Verify that when creating a group in GitLab, checking the box for **Create a Mattermost team for this group** also creates a team in Mattermost and the GitLab user is a member of that team.
- [ ] Create a test project within the group created in the previous step and initialize with a `README`.

- [ ] Verify Mattermost slash command operation:
  - [ ] Enable slash commands using [GitLab documentation](https://docs.gitlab.com/ee/user/project/integrations/mattermost_slash_commands.html#configure-automatically).
  - [ ] Test slash commands by creating a new issue from the Mattermost instance. After following the prompt to re-authorize, the issue should be successfully created in GitLab.

- [ ] Verify GitLab issue notification.

  - [ ] Configure incoming web hooks in Mattermost using the [GitLab Documentation](https://docs.gitlab.com/ee/user/project/integrations/mattermost.html). Note that you have to configure both Mattermost and GitLab.
  - [ ] Using the created web hook, follow the documentation for adding [notification support](https://docs.gitlab.com/ee/user/project/integrations/mattermost.html#configure-mattermost-to-receive-gitlab-notifications).
  - [ ] Create an issue in the test project. Verify that the notification for the issue appears in Mattermost for the GitLab user.

### Upgrade installation tests

Install a GitLab package from the previous, latest minor number release on a
fresh OS installation. Run the following actions, checks, and tests:

- [ ] Install GitLab package from the previous, latest minor number release:

  - [ ] Verify that package installation logs/output shows no errors.
  - [ ] Verify Mattermost version by running `/opt/gitlab/embedded/bin/mattermost version`.

- [ ] Edit `/etc/gitlab/gitlab.rb` and set:

  - `external_url`
  - `mattermost_external_url`

  Both URLs should point to the same system so that GitLab and Mattermost are co-located. Example:

  ```yaml
  external_url 'gitlab.example.com'
  mattermost_external_url 'mattermost.example.com'
  ```

- [ ] Run `gitlab-ctl reconfigure`.
- [ ] Connect to `gitlab.example.com`
- [ ] Navigate to `Admin>Settings>Network>Outbound requests` and add `mattermost.example.com` to `Local IP addresses and domain names that hooks and services can accesss` and click `Save changes`.
- [ ] Navigate to `mattermost.example.com`.
- [ ] Verify that single-sign on using GitLab credentials is working:

  1. Click on `or create and account with` GitLab.
  1. When presented with the `Authorize GitLab Mattermost to use your account?` page, click on `Authorize`.
  1. You should land on the `Select teams` page.

- [ ] Verify that when creating a group in GitLab, checking the box for **Create a Mattermost team for this group** also creates a team in Mattermost and the GitLab user is a member of that team.
- [ ] Create a test project within the group created in the previous step and initialize with a `README`.

- [ ] Verify Mattermost slash command operation:

  - [ ] Enable slash commands using [GitLab documentation](https://docs.gitlab.com/ee/user/project/integrations/mattermost_slash_commands.html#configure-automatically).
  - [ ] Test slash commands by creating a new issue from the Mattermost instance. After following the prompt to re-authorize, the issue should be successfully created in GitLab.

- [ ] Verify GitLab issue notification.

  - [ ] Configure incoming web hooks in Mattermost using the [GitLab Documentation](https://docs.gitlab.com/ee/user/project/integrations/mattermost.html). Note that you have to configure both Mattermost and GitLab.
  - [ ] Using the created web hook, follow the documentation for adding [notification support](https://docs.gitlab.com/ee/user/project/integrations/mattermost.html#configure-mattermost-to-receive-gitlab-notifications).
  - [ ] Create an issue in the test project. Verify that the notification for the issue appears in Mattermost for the GitLab user.

Upgrade GitLab with a package created with the new Mattermost version. Run the
following actions, checks, and tests:

- [ ] Upgraded to package with new Mattermost version:

  - [ ] Verify that package installation logs/output shows no errors.
  - [ ] Verify Mattermost version by running `/opt/gitlab/embedded/bin/mattermost version`.

- [ ] Verify Mattermost slash command operation:

  - [ ] Test slash commands by creating a new issue from the Mattermost instance. After following the prompt to re-authorize, the issue should be successfully created in GitLab.

- [ ] Verify GitLab issue notification.

  - [ ] Create an issue in the test project. Verify that the notification for the issue appears in Mattermost for the GitLab user.
